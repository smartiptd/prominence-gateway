#!/usr/bin/env node

/* Module dependencies */
var logger = require('smartdrive-lib').logger;
var common = require('smartdrive-lib').common;
var constant = require('smartdrive-lib').constant;
var mqttRegex = require('mqtt-regex');
var pkg = require('./package');
var async = require('async');
var auth = require('./auth');
var mqtt = require('mqtt');
var config = require('config'),
    conf = config.get('app');

var client;

/**********************
* AAF Gateway Process *
**********************/
var process = function(subsys) {
    try {
        auth.getInfo(function(err, _subsys, _token) {
            if (err) {
                logger.error(err);
            } else {
                if (_subsys && _token) {
                    if (!client) {
                        /* Prepare options for connection */
                        var opts = conf.mqtt.client.broker.options;
                        opts.clientId = conf.oauth.client.clientID;
                        opts.username = _token.access_token;
                        opts.password = common.encrypt(_token.challenge_num * common.randomInt(1000, 9999), conf.oauth.client.clientSignature);

                        var challengeText = common.randomStr(8);

                        /********************
                        * Connect to broker *
                        ********************/
                        client = mqtt.connect(conf.mqtt.client.broker.url, opts);
                        client.token = _token;
                        client.subsys = _subsys;
                        client.challengeText = challengeText;
                        client.secure = false;

                        client.on('connect', function() {
                            try {
                                logger.info('[' + pkg.name.toUpperCase() + ']', 'Connected to', conf.mqtt.client.broker.url);
                                async.each(_subsys, function(_tmpSubsys, cb) {
                                    /* Subscribe topic */
                                    var _subscribe = conf.mqtt.client.scope;
                                    _subscribe = _subscribe.replace('<SUBSYSTYPE>', _tmpSubsys.type);
                                    _subscribe = _subscribe.replace('<SUBSYSID>', _tmpSubsys.id);
                                    client.subscribe(_subscribe);
                                    logger.info('[' + pkg.name.toUpperCase() + ']', 'Subscribe:', _subscribe);
                                    cb();
                                }, function(err) {
                                    if (err) {
                                        logger.error(err);
                                    } else {
                                        /* Subscribe challenge MQTT broker result */
                                        var _subscribe = conf.mqtt.client.commonScope;
                                        _subscribe = _subscribe.replace('<CLIENTID>', conf.oauth.client.clientID);
                                        client.subscribe(_subscribe);
                                        logger.info('[' + pkg.name.toUpperCase() + ']', 'Subscribe:', _subscribe);
                                        /* Challenge MQTT broker */
                                        var _header = conf.mqtt.client.topic.common.header.replace('<ID>', conf.oauth.client.clientID);
                                        var _path = conf.mqtt.client.topic.common.path.challengeBroker;
                                        var _topic = _header + _path;
                                        var _msg = {
                                            challengeText: challengeText
                                        };
                                        client.publish(_topic, JSON.stringify(_msg));
                                        logger.info('[' + pkg.name.toUpperCase() + ']', 'Publish:', _topic, 'Message:', JSON.stringify(_msg));
                                    }
                                });
                            } catch (err) {
                                logger.error(err);
                            }
                        });

                        client.on('message', function(topic, message) {
                            try {
                                if (topic && message) {
                                    logger.info('[' + pkg.name.toUpperCase() + ']', 'Receive:', JSON.stringify({
                                        'topic': topic,
                                        'payload': JSON.parse(message.toString())
                                    }));
                                    /* Identify topic */
                                    var pattern = '/+subsystype/+subsysid/#path';
                                    var compiled = mqttRegex(pattern);
                                    var executed = compiled.regex.exec(topic);
                                    var info = compiled.getParams(executed);
                                    if (info) {
                                        var path = constant.SLASH + info.path.toString().replace(/[\,]/g, '\/');
                                        if (client.secure || path == conf.mqtt.client.topic.common.path.challengeBrokerReply) {
                                            switch (info.subsystype) {
                                                case 'common':
                                                    require('./execute/client/common')(client, message, info);
                                                    break;
                                                case 'aaf':
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                    }
                                }
                            } catch (err) {
                                logger.error(err);
                            }
                        });

                        client.on('reconnect', function(number) {
                            try {
                                logger.info('[' + pkg.name.toUpperCase() + ']', 'Try to connect to', conf.mqtt.client.broker.url);
                            } catch (err) {
                                logger.error(err);
                            }
                        });

                        client.on('offline', function(number) {
                            try {
                                logger.info('[' + pkg.name.toUpperCase() + ']', 'Broker offline');
                            } catch (err) {
                                logger.error(err);
                            }
                        });

                        client.on('close', function() {
                            try {
                                logger.info('[' + pkg.name.toUpperCase() + ']', 'Close connection');
                            } catch (err) {
                                logger.error(err);
                            }
                        });

                        client.on('error', function(err) {
                            try {
                                client.end(true, function() {
                                    /***** List of Error Messages *****
                                    # 'Unacceptable protocol version' #
                                    # 'Identifier rejected'           #
                                    # 'Server unavailable'            #
                                    # 'Bad username or password'      #
                                    # 'Not authorized'                #
                                    **********************************/
                                    var errMsg = err.toString();
                                    console.log(errMsg);
                                    if (errMsg.indexOf('Not authorized') >= 0) {
                                        auth.removeToken(function(err) {
                                            client = null;
                                        });
                                    } else {
                                        client = null;
                                    }
                                });
                            } catch (err) {
                                logger.error(err);
                            }
                        });
                    } else if (client.token != _token) {
                        client.token = _token;
                    } else if (client.subsys != _subsys) {
                        client = null;
                    }
                }
            }
        });
        setTimeout(process, conf.mqtt.client.updateInfo * 1000);
    } catch (err) {
        logger.error(err);
    }
}

module.exports.process = process;
