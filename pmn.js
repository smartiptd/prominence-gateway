#!/usr/bin/env node

/* Module dependencies */
var mongoose = require('mongoose')
var logger = require('smartdrive-lib').logger;
var pkg = require('./package');
var subsys = require('./subsys');
var config = require('config'),
	dbconf = config.get('db');

function start() {
	try {
		/* Connect to database */
		var conn = mongoose.connection;
		mongoose.connect('mongodb://' + dbconf.host + '/' + dbconf.name, {
			server: {
				socketOptions: {
					socketTimeoutMS: 300000,
					connectionTimeout: 30000
				}
			}
		});

		conn.once('connected', function () {
			try {
				logger.info('[' + pkg.name.toUpperCase() + ']', 'Database connected');

				/* Start MQTT processes */
				require('./broker').process(subsys);
				// require('./client').process(subsys);

			} catch (err) {
				logger.error(err);
			}
		});

		conn.on('disconnected', function () {
			try {
				logger.info('[' + pkg.name.toUpperCase() + ']', 'Database disconnected');
				setTimeout(function () {
					mongoose.connect('mongodb://' + dbconf.host + '/' + dbconf.name, {
						server: {
							socketOptions: {
								socketTimeoutMS: 300000,
								connectionTimeout: 30000
							}
						}
					});
				}, 10000);
			} catch (err) {
				logger.error(err);
			}
		});

		conn.on('error', function (err) {
			try {
				logger.error('[' + pkg.name.toUpperCase() + ']', 'Cannot connect to database', {
					error: err
				});
			} catch (err) {
				logger.error(err);
			}
		});
	});
} catch (err) {
	logger.error(err);
}


/* Start process */
start();
