'use strict';

/* Module dependencies */
var logger = require('smartdrive-lib').logger;
var constant = require('smartdrive-lib').constant;
var request = require('request');
var async = require('async');
var pkg = require('../../package');
var db = require('../../db');
var config = require('config'),
    conf = config.get('app');
var moment = require('moment');
var pmnConf = require('../../config/pmn').pmn;

/**
* The prominence process
*
* @param {Object} broker
* @param {Object} client
* @param {Object} packet
* @param {Object} info
*/
module.exports = function(broker, client, packet, info) {
    try {
        if (info.path) {
            var path = constant.SLASH + info.path.toString().replace(/[\,]/g, '\/');
            switch (path) {

                /* Update node status */
                case conf.mqtt.broker.topic.pmn.path.updateNodeStatus:
                    var _payload = JSON.parse(packet.payload.toString());
                    if (_payload && (_payload.electricity || _payload.water)) {
                        /* update electricity data */
                        updateElectricityData(_payload.electricity);
                        /* update water data */
                        updateWaterData(_payload.water);
                    }
                    break;

                default:
                    break;
            }
        }
    } catch (err) {
        logger.error(err);
    }
}

/* Local function */
var updateElectricityData = function(electricityDataList) {
    try {
        if (electricityDataList) {
            var updateFlagList = [];
            async.forEachOf(electricityDataList, function(data, channel, callback) {
                db.PMN.ElectricityData.findOne({channel:channel})
                .sort({_id:-1})
                .exec(function(err, _lastElectricityData) {
                    if (err) {
                        callback(err);
                    } else {
                        var usageKwh = ((_lastElectricityData) ? (
                            (parseFloat(data.kwh) >= parseFloat(_lastElectricityData.kwh)) ?
                            (parseFloat(data.kwh) - parseFloat(_lastElectricityData.kwh)) : parseFloat(data.kwh)
                        ) : 0).toFixed(2);
                        var timestamp = new Date();
                        var archiveDate = moment([
                            timestamp.getFullYear(),
                            timestamp.getMonth(),
                            timestamp.getDate(),
                            timestamp.getHours()
                        ]);
                        var archiveKey = archiveDate.format('YYYYMMDDHH');
                        /* save electricity data */
                        var _electricityData = new db.PMN.ElectricityData();
                        _electricityData.channel = data.channel = channel;
                        _electricityData.kwh = data.kwh = parseFloat(data.kwh).toFixed(2);
                        _electricityData.usageKwh = data.usageKwh = usageKwh;
                        _electricityData.timestamp = timestamp;
                        _electricityData.archiveKey = archiveKey;
                        _electricityData.refUsage = (_lastElectricityData) ? _lastElectricityData._id : null;
                        _electricityData.save(function(err) {
                            if (err) {
                                callback(err);
                            } else {
                                //logger.data(data);
                                callback(null);
                            }
                        });
                    }
                });
            }, function(err) {
                if (err) {
                    logger.error(err);
                }
            });
        }
    } catch (err) {
        logger.error(err);
    }
}

var updateWaterData = function(waterDataList) {
    try {
        if (waterDataList) {
            async.forEachOf(waterDataList, function(data, channel, callback) {
                db.PMN.WaterData.findOne({channel:channel})
                .sort({_id:-1})
                .exec(function(err, _lastWaterData) {
                    if (err) {
                        callback(err);
                    } else {
                        var usageUnit = ((_lastWaterData) ? (
                            (parseFloat(data.unit) >= parseFloat(_lastWaterData.unit)) ?
                            (parseFloat(data.unit) - parseFloat(_lastWaterData.unit)) : parseFloat(data.unit)
                        ) : 0).toFixed(2);
                        var timestamp = new Date();
                        var archiveDate = moment([
                            timestamp.getFullYear(),
                            timestamp.getMonth(),
                            timestamp.getDate(),
                            timestamp.getHours()
                        ]);
                        var archiveKey = archiveDate.format('YYYYMMDDHH');
                        /* save electricity data */
                        var _waterData = new db.PMN.WaterData();
                        _waterData.channel = data.channel = channel;
                        _waterData.unit = data.unit = parseFloat(data.unit).toFixed(2);
                        _waterData.usageUnit = data.usageUnit = usageUnit;
                        _waterData.timestamp = timestamp;
                        _waterData.archiveKey = archiveKey;
                        _waterData.refUsage = (_lastWaterData) ? _lastWaterData._id : null;
                        _waterData.save(function(err) {
                            if (err) {
                                callback(err);
                            } else {
                                //logger.data(data);
                                callback(null);
                            }
                        });
                    }
                });
            }, function(err) {
                if (err) {
                    logger.error(err);
                }
            });
        }
    } catch (err) {
        logger.error(err);
    }
}
