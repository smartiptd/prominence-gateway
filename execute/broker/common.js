'use strict';

/* Module dependencies */
var logger = require('smartdrive-lib').logger;
var constant = require('smartdrive-lib').constant;
var request = require('request');
var pkg = require('../../package');
var config = require('config'),
    conf = config.get('app');

/**
* Common process
*
* @param {Object} broker
* @param {Object} client
* @param {Object} packet
* @param {Object} info
*/
module.exports = function(broker, client, packet, info) {
    try {
        if (info.path) {
            var path = constant.SLASH + info.path.toString().replace(/[\,]/g, '\/');
            switch (path) {
                case '':
                    //TODO Add process
                    break;
                default:
                    break;
            }
        }
    } catch (err) {
        logger.error(err);
    }
}
