'use strict';

/* Module dependencies */
var logger = require('smartdrive-lib').logger;
var constant = require('smartdrive-lib').constant;
var common = require('smartdrive-lib').common;
var pkg = require('../../package');
var config = require('config'),
    conf = config.get('app');

/**
* Common process
*
* @param {Object} client
* @param {Object} message
* @param {Object} info
*/
module.exports = function(client, message, info) {
    try {
        var path = constant.SLASH + info.path.toString().replace(/[\,]/g, '\/');
        switch (path) {
            /* Challenge broker */
            case conf.mqtt.client.topic.common.path.challengeBrokerReply:
                var result = JSON.parse(message.toString());
                if (result && result.challengeCode) {
                    var challengeCode = result.challengeCode;
                    var challengeText = common.decrypt(challengeCode, conf.oauth.client.clientSignature);
                    if (client.challengeText != challengeText) {
                        logger.warn('[' + pkg.name.toUpperCase() + ']', 'Fail to challenge broker');
                        client.end();
                    } else {
                        logger.info('[' + pkg.name.toUpperCase() + ']', 'Challenge broker successful');
                        client.secure = true;
                    }
                }
                break;

            default:
                break;
        }
    } catch (err) {
        logger.error(err);
    }
}
