'use strict';

/* Module dependencies */
var logger = require('smartdrive-lib').logger;
var pkg = require('../package');
var config = require('config'),
    conf = config.get('app');

/******************
* Handler Process *
******************/
var handle = function(pmn, broker) {
    try {
        pmn.emitter.on('publishElectricityDailyData', function(data) {
            try {
                if (data) {
                    var topic = conf.mqtt.broker.topic.pmn.header.replace('<ID>', 'all') +
                        conf.mqtt.broker.topic.pmn.path.publishElectricityDailyData;
                    var message = {
                        topic: topic,
                        qos: 0,
                        retain: false,
                        payload: JSON.stringify(data)
                    };
                    broker.publish(message, function() {
                        //logger.info('[' + pkg.name.toUpperCase() + ']', 'Publish:', JSON.stringify(message));
                    });
                }
            } catch (err) {
                logger.error(err);
            }
        });

        pmn.emitter.on('publishElectricityMonthlyData', function(data) {
            try {
                if (data) {
                    var topic = conf.mqtt.broker.topic.pmn.header.replace('<ID>', 'all') +
                        conf.mqtt.broker.topic.pmn.path.publishElectricityMonthlyData;
                    var message = {
                        topic: topic,
                        qos: 0,
                        retain: false,
                        payload: JSON.stringify(data)
                    };
                    broker.publish(message, function() {
                        //logger.info('[' + pkg.name.toUpperCase() + ']', 'Publish:', JSON.stringify(message));
                    });
                }
            } catch (err) {
                logger.error(err);
            }
        });

        pmn.emitter.on('publishElectricityYearlyData', function(data) {
            try {
                if (data) {
                    var topic = conf.mqtt.broker.topic.pmn.header.replace('<ID>', 'all') +
                        conf.mqtt.broker.topic.pmn.path.publishElectricityYearlyData;
                    var message = {
                        topic: topic,
                        qos: 0,
                        retain: false,
                        payload: JSON.stringify(data)
                    };
                    broker.publish(message, function() {
                        //logger.info('[' + pkg.name.toUpperCase() + ']', 'Publish:', JSON.stringify(message));
                    });
                }
            } catch (err) {
                logger.error(err);
            }
        });

        pmn.emitter.on('publishElectricitySummaryData', function(data) {
            try {
                if (data) {
                    var topic = conf.mqtt.broker.topic.pmn.header.replace('<ID>', 'all') +
                        conf.mqtt.broker.topic.pmn.path.publishElectricitySummaryData;
                    var message = {
                        topic: topic,
                        qos: 0,
                        retain: false,
                        payload: JSON.stringify(data)
                    };
                    broker.publish(message, function() {
                        //logger.info('[' + pkg.name.toUpperCase() + ']', 'Publish:', JSON.stringify(message));
                    });
                }
            } catch (err) {
                logger.error(err);
            }
        });

        pmn.emitter.on('publishWaterDailyData', function(data) {
            try {
                if (data) {
                    var topic = conf.mqtt.broker.topic.pmn.header.replace('<ID>', 'all') +
                        conf.mqtt.broker.topic.pmn.path.publishWaterDailyData;
                    var message = {
                        topic: topic,
                        qos: 0,
                        retain: false,
                        payload: JSON.stringify(data)
                    };
                    broker.publish(message, function() {
                        //logger.info('[' + pkg.name.toUpperCase() + ']', 'Publish:', JSON.stringify(message));
                    });
                }
            } catch (err) {
                logger.error(err);
            }
        });

        pmn.emitter.on('publishWaterMonthlyData', function(data) {
            try {
                if (data) {
                    var topic = conf.mqtt.broker.topic.pmn.header.replace('<ID>', 'all') +
                        conf.mqtt.broker.topic.pmn.path.publishWaterMonthlyData;
                    var message = {
                        topic: topic,
                        qos: 0,
                        retain: false,
                        payload: JSON.stringify(data)
                    };
                    broker.publish(message, function() {
                        //logger.info('[' + pkg.name.toUpperCase() + ']', 'Publish:', JSON.stringify(message));
                    });
                }
            } catch (err) {
                logger.error(err);
            }
        });

        pmn.emitter.on('publishWaterYearlyData', function(data) {
            try {
                if (data) {
                    var topic = conf.mqtt.broker.topic.pmn.header.replace('<ID>', 'all') +
                        conf.mqtt.broker.topic.pmn.path.publishWaterYearlyData;
                    var message = {
                        topic: topic,
                        qos: 0,
                        retain: false,
                        payload: JSON.stringify(data)
                    };
                    broker.publish(message, function() {
                        //logger.info('[' + pkg.name.toUpperCase() + ']', 'Publish:', JSON.stringify(message));
                    });
                }
            } catch (err) {
                logger.error(err);
            }
        });

        pmn.emitter.on('publishWaterSummaryData', function(data) {
            try {
                if (data) {
                    var topic = conf.mqtt.broker.topic.pmn.header.replace('<ID>', 'all') +
                        conf.mqtt.broker.topic.pmn.path.publishWaterSummaryData;
                    var message = {
                        topic: topic,
                        qos: 0,
                        retain: false,
                        payload: JSON.stringify(data)
                    };
                    broker.publish(message, function() {
                        //logger.info('[' + pkg.name.toUpperCase() + ']', 'Publish:', JSON.stringify(message));
                    });
                }
            } catch (err) {
                logger.error(err);
            }
        });

    } catch (err) {
        logger.error(err);
    }
}

module.exports.handle = handle;
