'use strict';

/* Module dependencies */
var logger = require('smartdrive-lib').logger;

/******************
* Handler Process *
******************/
var create = function(subsys, broker) {
    try {
        /* Handle by subsys */
        require('./pmn').handle(subsys.PMN, broker);
        
    } catch (err) {
        logger.error(err);
    }
}

module.exports.create = create;
