'use strict';

/* Module dependencies */
var logger = require('smartdrive-lib').logger;
var mosca = require('mosca');
var mqttRegex = require('mqtt-regex');
var pkg = require('./package');
var auth = require('./auth');
var config = require('config'),
    conf = config.get('app');

var broker;

/**********************
* MQTT Broker Process *
**********************/
var process = function(subsys) {
    try {
        if (!broker) {

            /*********
            * Broker *
            *********/
            broker = new mosca.Server({
                interfaces: conf.mqtt.broker.interfaces
            });

            broker.authenticate = auth.clientAuthenticate();
            broker.authorizePublish = auth.clientAuthorizePublish();
            broker.authorizeSubscribe = auth.clientAuthorizeSubscribe();

            /* Broker event */
            broker.on('error', function(err) {
                try {
                    logger.error(err);
                    broker = null;
                    process(subsys);
                } catch (err) {
                    logger.error(err);
                }
            });

            broker.on('clientConnected', function(client) {
                try {
                    logger.info('[CLIENT:' + client.id + ']', 'Connected');
                } catch (err) {
                    logger.error(err);
                }
            });

            broker.on('published', function(packet, client) {
                try {
                    if (packet && client) {
                        logger.info('[' + pkg.name.toUpperCase() + ']', 'Receive:', JSON.stringify({
                            'topic': packet.topic,
                            'payload': JSON.parse(packet.payload.toString())
                        }));
                        /* Identify topic */
                        var pattern = '/+subsystype/+subsysid/#path';
                        var compiled = mqttRegex(pattern);
                        var executed = compiled.regex.exec(packet.topic);
                        var info = compiled.getParams(executed);
                        if (info) {
                            switch (info.subsystype) {
                                case 'common':
                                    require('./execute/broker/common')(broker, client, packet, info);
                                    break;
                                case 'aaf':
                                    require('./execute/broker/aaf')(broker, client, packet, info);
                                    break;
                                case 'pmn':
                                    require('./execute/broker/pmn')(broker, client, packet, info);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                } catch (err) {
                    logger.error(err);
                }
            });

            broker.on('subscribed', function(topic, client) {
                try {
                    //TODO add feature
                } catch (err) {
                    logger.error(err);
                }
            });

            broker.on('clientDisconnected', function(client) {
                try {
                    logger.info('[CLIENT:' + client.id + ']', 'Disconnected');
                } catch (err) {
                    logger.error(err);
                }
            });

            /* Start broker */
            broker.on('ready', function() {
                try {
                    logger.info('[' + pkg.name.toUpperCase() + ']', 'Listening on port',
                                conf.mqtt.broker.interfaces[0].port, 'and',
                                conf.mqtt.broker.interfaces[1].port);
                    require('./handler').create(subsys, broker);
                } catch (err) {
                    logger.error(err);
                }
            });
        }
        setTimeout(process, conf.mqtt.broker.updateInfo * 1000);
    } catch (err) {
        logger.error(err);
    }
}

module.exports.process = process;
