'use strict';

/* Module dependencies */
var db =  require('../db');
var logger = require('smartdrive-lib').logger;
var config = require('config'),
    conf = config.get('app');
var EventEmitter = require('events').EventEmitter,
    emitter = new EventEmitter;
var pkg = require('../package');
var async = require('async');
var moment = require('moment');
var pmnConf = require('../config/pmn').pmn;

var PMN = function() {
    try {

    } catch (err) {
        logger.error(err);
    }
};

PMN.prototype.emitter = emitter;

/*************************
* The Prominence Process *
*************************/
/* Electricity */
var archiveElectricityData = function() {
    try {
        setTimeout(archiveElectricityData , conf.subsys.pmn.archiveInterval * 1000);
        var _channelList = pmnConf.electricity.channel;
        if (_channelList) {
            /* Loop all channels */
            var maxTimestamp;
            async.each(_channelList, function(_channel, done) {
                /* Find non archived record */
                db.PMN.ElectricityData.find({channel:_channel, archiveFlag:false})
                .sort({timestamp:1})
                .exec(function(err, _archiveDataList) {
                    if (err) {
                        done(err);
                    } else {
                        if (_archiveDataList && _archiveDataList.length > 0) {
                            /* Prepare search key */
                            var dailyArchiveKeyList = [];
                            var monthlyArchiveKeyList = [];
                            var yearlyArchiveKeyList = [];
                            var summaryArchiveKeyList = [];
                            async.each(_archiveDataList, function(_archiveData, done2) {
                                maxTimestamp = (!maxTimestamp || maxTimestamp < _archiveData.timestamp) ?
                                    _archiveData.timestamp : maxTimestamp;
                                var _timestamp = moment([
                                    _archiveData.timestamp.getFullYear(),
                                    _archiveData.timestamp.getMonth(),
                                    _archiveData.timestamp.getDate(),
                                    _archiveData.timestamp.getHours()
                                ]);
                                var dailyArchiveKey = _timestamp.format('YYYYMMDDHH');
                                var monthlyArchiveKey = _timestamp.format('YYYYMMDD');
                                var yearlyArchiveKey = _timestamp.format('YYYYMM');
                                var summaryArchiveKey = _timestamp.format('YYYY');
                                if (dailyArchiveKeyList.indexOf(dailyArchiveKey) == -1)
                                    dailyArchiveKeyList.push(dailyArchiveKey);
                                if (monthlyArchiveKeyList.indexOf(monthlyArchiveKey) == -1)
                                    monthlyArchiveKeyList.push(monthlyArchiveKey);
                                if (yearlyArchiveKeyList.indexOf(yearlyArchiveKey) == -1)
                                    yearlyArchiveKeyList.push(yearlyArchiveKey);
                                if (summaryArchiveKeyList.indexOf(summaryArchiveKey) == -1)
                                    summaryArchiveKeyList.push(summaryArchiveKey);
                                done2(null);
                            }, function(err) {
                                if (err) {
                                    done(err);
                                } else {
                                    async.waterfall([
                                        function(next) {
                                            archiveElectricityDailyData(_channel, dailyArchiveKeyList, next);
                                        },
                                        function(next) {
                                            archiveElectricityMonthlyData(_channel, monthlyArchiveKeyList, next);
                                        },
                                        function(next) {
                                            archiveElectricityYearlyData(_channel, yearlyArchiveKeyList, next);
                                        },
                                        function(next) {
                                            archiveElectricitySummaryData(_channel, summaryArchiveKeyList, next);
                                        }
                                    ], function(err) {
                                        if (err) {
                                            done(err);
                                        } else {
                                            done(null);
                                        }
                                    });
                                }
                            });
                        } else {
                            done(null);
                        }
                    }
                });
            }, function(err) {
                if (err) {
                    logger.error(err);
                }  else {
                    db.PMN.ElectricityData.update({archiveFlag:false, timestamp:{$lte:maxTimestamp}},
                        {archiveFlag:true},
                        {multi:true},
                        function(err) {
                        if (err) {
                            logger.error(err);
                        } else {
                            logger.info('[' + pkg.name.toUpperCase() + ']', 'Electricity data archived successfully');
                        }
                    });
                }
            });
        }
    } catch (err) {
        logger.error(err);
    }
}

var archiveElectricityDailyData = function(channel, archiveKeyList, callback) {
    try {
        if (channel && archiveKeyList) {
            /* Loop all archive keys */
            var archiveFlag = false;
            async.each(archiveKeyList, function(_archiveKey, done) {
                /* Search data for archiving */
                db.PMN.ElectricityData.find({channel:channel, archiveKey:_archiveKey})
                .sort({timestamp:1})
                .exec(function(err, _archiveDataList) {
                    if (err) {
                        done(err);
                    } else {
                        if (_archiveDataList && _archiveDataList.length > 0) {
                            /* Archive data */
                            var archiveObj = {};
                            async.each(_archiveDataList, function(_archiveData, done2) {
                                archiveObj.channel = _archiveData.channel;
                                archiveObj.sumKwh =  (archiveObj.sumKwh) ?
                                    (parseFloat(archiveObj.sumKwh) + parseFloat(_archiveData.usageKwh)).toFixed(2) : _archiveData.usageKwh;
                                archiveObj.archiveDate = moment(_archiveKey, 'YYYYMMDDHH');
                                archiveObj.archiveKey = moment(_archiveKey, 'YYYYMMDDHH').format('YYYYMMDD');
                                done2(null);
                            }, function(err) {
                                if (err) {
                                    done(err);
                                } else {
                                    /* Update archive data into database */
                                    db.PMN.ElectricityDailyData.update({
                                        channel:archiveObj.channel,
                                        archiveDate:archiveObj.archiveDate
                                    },
                                    {$set:{
                                        channel:archiveObj.channel,
                                        sumKwh:archiveObj.sumKwh,
                                        archiveDate:archiveObj.archiveDate,
                                        archiveKey:archiveObj.archiveKey
                                    }},
                                    {upsert:true},
                                    function(err, stat) {
                                        if (err) {
                                            done(err);
                                        } else {
                                            if (stat.n > 0) archiveFlag = true;
                                            done(null);
                                        }
                                    });
                                }
                            });
                        } else {
                            done(null);
                        }
                    }
                });
            }, function(err) {
                if (err) {
                    callback(err);
                } else {
                    if (archiveFlag) {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.ElectricityData\' completed', '[' + channel + ']');
                    } else {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.ElectricityData\' ignored', '[' + channel + ']');
                    }
                    callback(null);
                }
            });
        } else {
            callback(null);
        }
    } catch (err) {
        callback(err);
    }
}

var archiveElectricityMonthlyData = function(channel, archiveKeyList, callback) {
    try {
        if (channel && archiveKeyList) {
            /* Loop all archive keys */
            var archiveFlag = false;
            async.each(archiveKeyList, function(_archiveKey, done) {
                /* Search data for archiving */
                db.PMN.ElectricityDailyData.find({channel:channel, archiveKey:_archiveKey})
                .sort({timestamp:1})
                .exec(function(err, _archiveDataList) {
                    if (err) {
                        done(err);
                    } else {
                        if (_archiveDataList && _archiveDataList.length > 0) {
                            /* Archive data */
                            var archiveObj = {};
                            async.each(_archiveDataList, function(_archiveData, done2) {
                                archiveObj.channel = _archiveData.channel;
                                archiveObj.sumKwh =  (archiveObj.sumKwh) ?
                                    (parseFloat(archiveObj.sumKwh) + parseFloat(_archiveData.sumKwh)).toFixed(2) : _archiveData.sumKwh;
                                archiveObj.archiveDate = moment(_archiveKey, 'YYYYMMDD');
                                archiveObj.archiveKey = moment(_archiveKey, 'YYYYMMDD').format('YYYYMM');
                                done2(null);
                            }, function(err) {
                                if (err) {
                                    done(err);
                                } else {
                                    /* Update archive data into database */
                                    db.PMN.ElectricityMonthlyData.update({
                                        channel:archiveObj.channel,
                                        archiveDate:archiveObj.archiveDate
                                    },
                                    {$set:{
                                        channel:archiveObj.channel,
                                        sumKwh:archiveObj.sumKwh,
                                        archiveDate:archiveObj.archiveDate,
                                        archiveKey:archiveObj.archiveKey
                                    }},
                                    {upsert:true},
                                    function(err, stat) {
                                        if (err) {
                                            done(err);
                                        } else {
                                            if (stat.n > 0) archiveFlag = true;
                                            done(null);
                                        }
                                    });
                                }
                            });
                        } else {
                            done(null);
                        }
                    }
                });
            }, function(err) {
                if (err) {
                    callback(err);
                } else {
                    if (archiveFlag) {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.ElectricityDailyData\' completed', '[' + channel + ']');
                    } else {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.ElectricityDailyData\' ignored', '[' + channel + ']');
                    }
                    callback(null);
                }
            });
        } else {
            callback(null);
        }
    } catch (err) {
        callback(err);
    }
}

var archiveElectricityYearlyData = function(channel, archiveKeyList, callback) {
    try {
        if (channel && archiveKeyList) {
            /* Loop all archive keys */
            var archiveFlag = false;
            async.each(archiveKeyList, function(_archiveKey, done) {
                /* Search data for archiving */
                db.PMN.ElectricityMonthlyData.find({channel:channel, archiveKey:_archiveKey})
                .sort({timestamp:1})
                .exec(function(err, _archiveDataList) {
                    if (err) {
                        done(err);
                    } else {
                        if (_archiveDataList && _archiveDataList.length > 0) {
                            /* Archive data */
                            var archiveObj = {};
                            async.each(_archiveDataList, function(_archiveData, done2) {
                                archiveObj.channel = _archiveData.channel;
                                archiveObj.sumKwh =  (archiveObj.sumKwh) ?
                                    (parseFloat(archiveObj.sumKwh) + parseFloat(_archiveData.sumKwh)).toFixed(2) : _archiveData.sumKwh;
                                archiveObj.archiveDate = moment(_archiveKey, 'YYYYMM');
                                archiveObj.archiveKey = moment(_archiveKey, 'YYYYMM').format('YYYY');
                                done2(null);
                            }, function(err) {
                                if (err) {
                                    done(err);
                                } else {
                                    /* Update archive data into database */
                                    db.PMN.ElectricityYearlyData.update({
                                        channel:archiveObj.channel,
                                        archiveDate:archiveObj.archiveDate
                                    },
                                    {$set:{
                                        channel:archiveObj.channel,
                                        sumKwh:archiveObj.sumKwh,
                                        archiveDate:archiveObj.archiveDate,
                                        archiveKey:archiveObj.archiveKey
                                    }},
                                    {upsert:true},
                                    function(err, stat) {
                                        if (err) {
                                            done(err);
                                        } else {
                                            if (stat.n > 0) archiveFlag = true;
                                            done(null);
                                        }
                                    });
                                }
                            });
                        } else {
                            done(null);
                        }
                    }
                });
            }, function(err) {
                if (err) {
                    callback(err);
                } else {
                    if (archiveFlag) {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.ElectricityMonthlyData\' completed', '[' + channel + ']');
                    } else {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.ElectricityMonthlyData\' ignored', '[' + channel + ']');
                    }
                    callback(null);
                }
            });
        } else {
            callback(null);
        }
    } catch (err) {
        callback(err);
    }
}

var archiveElectricitySummaryData = function(channel, archiveKeyList, callback) {
    try {
        if (channel && archiveKeyList) {
            /* Loop all archive keys */
            var archiveFlag = false;
            async.each(archiveKeyList, function(_archiveKey, done) {
                /* Search data for archiving */
                db.PMN.ElectricityYearlyData.find({channel:channel, archiveKey:_archiveKey})
                .sort({timestamp:1})
                .exec(function(err, _archiveDataList) {
                    if (err) {
                        done(err);
                    } else {
                        if (_archiveDataList && _archiveDataList.length > 0) {
                            /* Archive data */
                            var archiveObj = {};
                            async.each(_archiveDataList, function(_archiveData, done2) {
                                archiveObj.channel = _archiveData.channel;
                                archiveObj.sumKwh =  (archiveObj.sumKwh) ?
                                    (parseFloat(archiveObj.sumKwh) + parseFloat(_archiveData.sumKwh)).toFixed(2) : _archiveData.sumKwh;
                                archiveObj.archiveDate = moment(_archiveKey, 'YYYY');
                                done2(null);
                            }, function(err) {
                                if (err) {
                                    done(err);
                                } else {
                                    /* Update archive data into database */
                                    db.PMN.ElectricitySummaryData.update({
                                        channel:archiveObj.channel,
                                        archiveDate:archiveObj.archiveDate
                                    },
                                    {$set:{
                                        channel:archiveObj.channel,
                                        sumKwh:archiveObj.sumKwh,
                                        archiveDate:archiveObj.archiveDate
                                    }},
                                    {upsert:true},
                                    function(err, stat) {
                                        if (err) {
                                            done(err);
                                        } else {
                                            if (stat.n > 0) archiveFlag = true;
                                            done(null);
                                        }
                                    });
                                }
                            });
                        } else {
                            done(null);
                        }
                    }
                });
            }, function(err) {
                if (err) {
                    callback(err);
                } else {
                    if (archiveFlag) {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.ElectricityYearlyData\' completed', '[' + channel + ']');
                    } else {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.ElectricityYearlyData\' ignored', '[' + channel + ']');
                    }
                    callback(null);
                }
            });
        } else {
            callback(null);
        }
    } catch (err) {
        callback(err);
    }
}

var clearElectricityData = function() {
    try {
        setTimeout(clearElectricityData, conf.subsys.pmn.clearArchiveInterval * 1000);
        /* Clear archived data */
        db.PMN.ElectricityData.remove({timestamp:{$lt:moment().subtract(1, 'years')}, 'archiveFlag':true}, function(err, stat) {
            if (err) {
                logger.error(err);
            } else {
                logger.info('[' + pkg.name.toUpperCase() + ']', 'Remove', stat.result.n, 'record(s)', 'of \'pmn.ElectricityData\'');
            }
        });
    } catch (err) {
        logger.error(err);
    }
}

var publishElectricityDailyData = function() {
    try {
        setTimeout(publishElectricityDailyData, conf.subsys.pmn.publishDataInterval * 1000);

        /* Prepare JSON schema */
        var electricityDailyDataObj = {
            electricityDailyData: {
                labels: [],
                datasets: {}
            }
        };

        /* Prepare date range */
        var now = new Date();
        var endDate = moment([now.getFullYear(), now.getMonth(), now.getDate(), now.getHours()]);
        var beginDate = endDate.clone().subtract(1, 'days');

        /* Get data & Prepare data */
        var runningDate = beginDate.clone().add(1, 'hours');
        var labels = [];
        var datasets = {};
        async.whilst(
            function() { return runningDate.isSameOrBefore(endDate); },
            function(done) {
                labels.push(runningDate.format('HH:mm'));
                var channelList = pmnConf.electricity.channel;
                async.each(channelList, function(_channel, done2) {
                    db.PMN.ElectricityDailyData.findOne({archiveDate:runningDate, channel:_channel})
                    .exec(function(err, _archiveData) {
                        if (err) {
                            done2(err);
                        } else {
                            var sumKwh = (_archiveData) ? parseFloat(_archiveData.sumKwh) : 0;
                            if (datasets[_channel]) {
                                datasets[_channel].push(sumKwh);
                            } else {
                                datasets[_channel] = [sumKwh];
                            }
                            done2(null);
                        }
                    });
                }, function(err) {
                    if (err) {
                        done(err);
                    } else {
                        /* Map object */
                        electricityDailyDataObj.electricityDailyData.timestamp = moment();
                        electricityDailyDataObj.electricityDailyData.labels = labels;
                        electricityDailyDataObj.electricityDailyData.datasets = datasets;
                        runningDate.add(1, 'hours');
                        done(null);
                    }
                });
            },
            function(err) {
                if (err) {
                    logger.error(err);
                } else {
                    emitter.emit('publishElectricityDailyData', electricityDailyDataObj);
                }
            }
        );
    } catch (err) {
        logger.error(err);
    }
}

var publishElectricityMonthlyData = function() {
    try {
        setTimeout(publishElectricityMonthlyData, conf.subsys.pmn.publishDataInterval * 1000);

        /* Prepare JSON schema */
        var electricityMonthlyDataObj = {
            electricityMonthlyData: {
                labels: [],
                datasets: {}
            }
        };

        /* Prepare date range */
        var endDate = moment().add(1, 'months').startOf('month');
        var beginDate = moment().startOf('month');

        /* Get data & Prepare data */
        var runningDate = beginDate.clone();
        var labels = [];
        var datasets = {};
        async.whilst(
            function() { return runningDate.isBefore(endDate); },
            function(done) {
                labels.push(runningDate.format('D'));
                var channelList = pmnConf.electricity.channel;
                async.each(channelList, function(_channel, done2) {
                    db.PMN.ElectricityMonthlyData.findOne({archiveDate:runningDate, channel:_channel})
                    .exec(function(err, _archiveData) {
                        if (err) {
                            done2(err);
                        } else {
                            var sumKwh = (_archiveData) ? parseFloat(_archiveData.sumKwh) : 0;
                            if (datasets[_channel]) {
                                datasets[_channel].push(sumKwh);
                            } else {
                                datasets[_channel] = [sumKwh];
                            }
                            done2(null);
                        }
                    });
                }, function(err) {
                    if (err) {
                        done(err);
                    } else {
                        /* Map object */
                        electricityMonthlyDataObj.electricityMonthlyData.timestamp = moment();
                        electricityMonthlyDataObj.electricityMonthlyData.labels = labels;
                        electricityMonthlyDataObj.electricityMonthlyData.datasets = datasets;
                        runningDate.add(1, 'days');
                        done(null);
                    }
                });
            },
            function(err) {
                if (err) {
                    logger.error(err);
                } else {
                    emitter.emit('publishElectricityMonthlyData', electricityMonthlyDataObj);
                }
            }
        );
    } catch (err) {
        logger.error(err);
    }
}

var publishElectricityYearlyData = function() {
    try {
        setTimeout(publishElectricityYearlyData, conf.subsys.pmn.publishDataInterval * 1000);

        /* Prepare JSON schema */
        var electricityYearlyDataObj = {
            electricityYearlyData: {
                labels: [],
                datasets: {}
            }
        };

        /* Prepare date range */
        var endDate = moment().add(1, 'years').startOf('year');
        var beginDate = moment().startOf('year');

        /* Get data & Prepare data */
        var runningDate = beginDate.clone();
        var labels = [];
        var datasets = {};
        async.whilst(
            function() { return runningDate.isBefore(endDate); },
            function(done) {
                labels.push(runningDate.format('MMM').toUpperCase());
                var channelList = pmnConf.electricity.channel;
                async.each(channelList, function(_channel, done2) {
                    db.PMN.ElectricityYearlyData.findOne({archiveDate:runningDate, channel:_channel})
                    .exec(function(err, _archiveData) {
                        if (err) {
                            done2(err);
                        } else {
                            var sumKwh = (_archiveData) ? parseFloat(_archiveData.sumKwh) : 0;
                            if (datasets[_channel]) {
                                datasets[_channel].push(sumKwh);
                            } else {
                                datasets[_channel] = [sumKwh];
                            }
                            done2(null);
                        }
                    });
                }, function(err) {
                    if (err) {
                        done(err);
                    } else {
                        /* Map object */
                        electricityYearlyDataObj.electricityYearlyData.timestamp = moment();
                        electricityYearlyDataObj.electricityYearlyData.labels = labels;
                        electricityYearlyDataObj.electricityYearlyData.datasets = datasets;
                        runningDate.add(1, 'months');
                        done(null);
                    }
                });
            },
            function(err) {
                if (err) {
                    logger.error(err);
                } else {
                    emitter.emit('publishElectricityYearlyData', electricityYearlyDataObj);
                }
            }
        );
    } catch (err) {
        logger.error(err);
    }
}

var publishElectricitySummaryData = function() {
    try {
        setTimeout(publishElectricitySummaryData, conf.subsys.pmn.publishDataInterval * 1000);

        /* Prepare JSON schema */
        var electricitySummaryDataObj = {
            electricitySummaryData: {
                labels: [],
                datasets: {}
            }
        };

        /* Prepare date range */
        var endDate = moment().add(1, 'years').startOf('year');
        var beginDate = moment().subtract(4, 'years').startOf('year');

        /* Get data & Prepare data */
        var runningDate = beginDate.clone();
        var labels = [];
        var datasets = {};
        async.whilst(
            function() { return runningDate.isBefore(endDate); },
            function(done) {
                labels.push(runningDate.format('YYYY').toUpperCase());
                var channelList = pmnConf.electricity.channel;
                async.each(channelList, function(_channel, done2) {
                    db.PMN.ElectricitySummaryData.findOne({archiveDate:runningDate, channel:_channel})
                    .exec(function(err, _archiveData) {
                        if (err) {
                            done2(err);
                        } else {
                            var sumKwh = (_archiveData) ? parseFloat(_archiveData.sumKwh) : 0;
                            if (datasets[_channel]) {
                                datasets[_channel].push(sumKwh);
                            } else {
                                datasets[_channel] = [sumKwh];
                            }
                            done2(null);
                        }
                    });
                }, function(err) {
                    if (err) {
                        done(err);
                    } else {
                        /* Map object */
                        electricitySummaryDataObj.electricitySummaryData.timestamp = moment();
                        electricitySummaryDataObj.electricitySummaryData.labels = labels;
                        electricitySummaryDataObj.electricitySummaryData.datasets = datasets;
                        runningDate.add(1, 'years');
                        done(null);
                    }
                });
            },
            function(err) {
                if (err) {
                    logger.error(err);
                } else {
                    emitter.emit('publishElectricitySummaryData', electricitySummaryDataObj);
                }
            }
        );
    } catch (err) {
        logger.error(err);
    }
}

/* Water */
var archiveWaterData = function() {
    try {
        setTimeout(archiveWaterData , conf.subsys.pmn.archiveInterval * 1000);
        var _channelList = pmnConf.water.channel;
        if (_channelList) {
            /* Loop all channels */
            var maxTimestamp;
            async.each(_channelList, function(_channel, done) {
                /* Find non archived record */
                db.PMN.WaterData.find({channel:_channel, archiveFlag:false})
                .sort({timestamp:1})
                .exec(function(err, _archiveDataList) {
                    if (err) {
                        done(err);
                    } else {
                        if (_archiveDataList && _archiveDataList.length > 0) {
                            /* Prepare search key */
                            var dailyArchiveKeyList = [];
                            var monthlyArchiveKeyList = [];
                            var yearlyArchiveKeyList = [];
                            var summaryArchiveKeyList = [];
                            async.each(_archiveDataList, function(_archiveData, done2) {
                                maxTimestamp = (!maxTimestamp || maxTimestamp < _archiveData.timestamp) ?
                                    _archiveData.timestamp : maxTimestamp;
                                var _timestamp = moment([
                                    _archiveData.timestamp.getFullYear(),
                                    _archiveData.timestamp.getMonth(),
                                    _archiveData.timestamp.getDate(),
                                    _archiveData.timestamp.getHours()
                                ]);
                                var dailyArchiveKey = _timestamp.format('YYYYMMDDHH');
                                var monthlyArchiveKey = _timestamp.format('YYYYMMDD');
                                var yearlyArchiveKey = _timestamp.format('YYYYMM');
                                var summaryArchiveKey = _timestamp.format('YYYY');
                                if (dailyArchiveKeyList.indexOf(dailyArchiveKey) == -1)
                                    dailyArchiveKeyList.push(dailyArchiveKey);
                                if (monthlyArchiveKeyList.indexOf(monthlyArchiveKey) == -1)
                                    monthlyArchiveKeyList.push(monthlyArchiveKey);
                                if (yearlyArchiveKeyList.indexOf(yearlyArchiveKey) == -1)
                                    yearlyArchiveKeyList.push(yearlyArchiveKey);
                                if (summaryArchiveKeyList.indexOf(summaryArchiveKey) == -1)
                                    summaryArchiveKeyList.push(summaryArchiveKey);
                                done2(null);
                            }, function(err) {
                                if (err) {
                                    done(err);
                                } else {
                                    async.waterfall([
                                        function(next) {
                                            archiveWaterDailyData(_channel, dailyArchiveKeyList, next);
                                        },
                                        function(next) {
                                            archiveWaterMonthlyData(_channel, monthlyArchiveKeyList, next);
                                        },
                                        function(next) {
                                            archiveWaterYearlyData(_channel, yearlyArchiveKeyList, next);
                                        },
                                        function(next) {
                                            archiveWaterSummaryData(_channel, summaryArchiveKeyList, next);
                                        }
                                    ], function(err) {
                                        if (err) {
                                            done(err);
                                        } else {
                                            done(null);
                                        }
                                    });
                                }
                            });
                        } else {
                            done(null);
                        }
                    }
                });
            }, function(err) {
                if (err) {
                    logger.error(err);
                }  else {
                    db.PMN.WaterData.update({archiveFlag:false, timestamp:{$lte:maxTimestamp}},
                        {archiveFlag:true},
                        {multi:true},
                        function(err) {
                        if (err) {
                            logger.error(err);
                        } else {
                            logger.info('[' + pkg.name.toUpperCase() + ']', 'Water data archived successfully');
                        }
                    });
                }
            });
        }
    } catch (err) {
        logger.error(err);
    }
}

var archiveWaterDailyData = function(channel, archiveKeyList, callback) {
    try {
        if (channel && archiveKeyList) {
            /* Loop all archive keys */
            var archiveFlag = false;
            async.each(archiveKeyList, function(_archiveKey, done) {
                /* Search data for archiving */
                db.PMN.WaterData.find({channel:channel, archiveKey:_archiveKey})
                .sort({timestamp:1})
                .exec(function(err, _archiveDataList) {
                    if (err) {
                        done(err);
                    } else {
                        if (_archiveDataList && _archiveDataList.length > 0) {
                            /* Archive data */
                            var archiveObj = {};
                            async.each(_archiveDataList, function(_archiveData, done2) {
                                archiveObj.channel = _archiveData.channel;
                                archiveObj.sumUnit =  (archiveObj.sumUnit) ?
                                    (parseFloat(archiveObj.sumUnit) + parseFloat(_archiveData.usageUnit)).toFixed(2) : _archiveData.usageUnit;
                                archiveObj.archiveDate = moment(_archiveKey, 'YYYYMMDDHH');
                                archiveObj.archiveKey = moment(_archiveKey, 'YYYYMMDDHH').format('YYYYMMDD');
                                done2(null);
                            }, function(err) {
                                if (err) {
                                    done(err);
                                } else {
                                    /* Update archive data into database */
                                    db.PMN.WaterDailyData.update({
                                        channel:archiveObj.channel,
                                        archiveDate:archiveObj.archiveDate
                                    },
                                    {$set:{
                                        channel:archiveObj.channel,
                                        sumUnit:archiveObj.sumUnit,
                                        archiveDate:archiveObj.archiveDate,
                                        archiveKey:archiveObj.archiveKey
                                    }},
                                    {upsert:true},
                                    function(err, stat) {
                                        if (err) {
                                            done(err);
                                        } else {
                                            if (stat.n > 0) archiveFlag = true;
                                            done(null);
                                        }
                                    });
                                }
                            });
                        } else {
                            done(null);
                        }
                    }
                });
            }, function(err) {
                if (err) {
                    callback(err);
                } else {
                    if (archiveFlag) {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.WaterData\' completed', '[' + channel + ']');
                    } else {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.WaterData\' ignored', '[' + channel + ']');
                    }
                    callback(null);
                }
            });
        } else {
            callback(null);
        }
    } catch (err) {
        callback(err);
    }
}

var archiveWaterMonthlyData = function(channel, archiveKeyList, callback) {
    try {
        if (channel && archiveKeyList) {
            /* Loop all archive keys */
            var archiveFlag = false;
            async.each(archiveKeyList, function(_archiveKey, done) {
                /* Search data for archiving */
                db.PMN.WaterDailyData.find({channel:channel, archiveKey:_archiveKey})
                .sort({timestamp:1})
                .exec(function(err, _archiveDataList) {
                    if (err) {
                        done(err);
                    } else {
                        if (_archiveDataList && _archiveDataList.length > 0) {
                            /* Archive data */
                            var archiveObj = {};
                            async.each(_archiveDataList, function(_archiveData, done2) {
                                archiveObj.channel = _archiveData.channel;
                                archiveObj.sumUnit =  (archiveObj.sumUnit) ?
                                    (parseFloat(archiveObj.sumUnit) + parseFloat(_archiveData.sumUnit)).toFixed(2) : _archiveData.sumUnit;
                                archiveObj.archiveDate = moment(_archiveKey, 'YYYYMMDD');
                                archiveObj.archiveKey = moment(_archiveKey, 'YYYYMMDD').format('YYYYMM');
                                done2(null);
                            }, function(err) {
                                if (err) {
                                    done(err);
                                } else {
                                    /* Update archive data into database */
                                    db.PMN.WaterMonthlyData.update({
                                        channel:archiveObj.channel,
                                        archiveDate:archiveObj.archiveDate
                                    },
                                    {$set:{
                                        channel:archiveObj.channel,
                                        sumUnit:archiveObj.sumUnit,
                                        archiveDate:archiveObj.archiveDate,
                                        archiveKey:archiveObj.archiveKey
                                    }},
                                    {upsert:true},
                                    function(err, stat) {
                                        if (err) {
                                            done(err);
                                        } else {
                                            if (stat.n > 0) archiveFlag = true;
                                            done(null);
                                        }
                                    });
                                }
                            });
                        } else {
                            done(null);
                        }
                    }
                });
            }, function(err) {
                if (err) {
                    callback(err);
                } else {
                    if (archiveFlag) {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.WaterDailyData\' completed', '[' + channel + ']');
                    } else {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.WaterDailyData\' ignored', '[' + channel + ']');
                    }
                    callback(null);
                }
            });
        } else {
            callback(null);
        }
    } catch (err) {
        callback(err);
    }
}

var archiveWaterYearlyData = function(channel, archiveKeyList, callback) {
    try {
        if (channel && archiveKeyList) {
            /* Loop all archive keys */
            var archiveFlag = false;
            async.each(archiveKeyList, function(_archiveKey, done) {
                /* Search data for archiving */
                db.PMN.WaterMonthlyData.find({channel:channel, archiveKey:_archiveKey})
                .sort({timestamp:1})
                .exec(function(err, _archiveDataList) {
                    if (err) {
                        done(err);
                    } else {
                        if (_archiveDataList && _archiveDataList.length > 0) {
                            /* Archive data */
                            var archiveObj = {};
                            async.each(_archiveDataList, function(_archiveData, done2) {
                                archiveObj.channel = _archiveData.channel;
                                archiveObj.sumUnit =  (archiveObj.sumUnit) ?
                                    (parseFloat(archiveObj.sumUnit) + parseFloat(_archiveData.sumUnit)).toFixed(2) : _archiveData.sumUnit;
                                archiveObj.archiveDate = moment(_archiveKey, 'YYYYMM');
                                archiveObj.archiveKey = moment(_archiveKey, 'YYYYMM').format('YYYY');
                                done2(null);
                            }, function(err) {
                                if (err) {
                                    done(err);
                                } else {
                                    /* Update archive data into database */
                                    db.PMN.WaterYearlyData.update({
                                        channel:archiveObj.channel,
                                        archiveDate:archiveObj.archiveDate
                                    },
                                    {$set:{
                                        channel:archiveObj.channel,
                                        sumUnit:archiveObj.sumUnit,
                                        archiveDate:archiveObj.archiveDate,
                                        archiveKey:archiveObj.archiveKey
                                    }},
                                    {upsert:true},
                                    function(err, stat) {
                                        if (err) {
                                            done(err);
                                        } else {
                                            if (stat.n > 0) archiveFlag = true;
                                            done(null);
                                        }
                                    });
                                }
                            });
                        } else {
                            done(null);
                        }
                    }
                });
            }, function(err) {
                if (err) {
                    callback(err);
                } else {
                    if (archiveFlag) {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.WaterMonthlyData\' completed', '[' + channel + ']');
                    } else {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.WaterMonthlyData\' ignored', '[' + channel + ']');
                    }
                    callback(null);
                }
            });
        } else {
            callback(null);
        }
    } catch (err) {
        callback(err);
    }
}

var archiveWaterSummaryData = function(channel, archiveKeyList, callback) {
    try {
        if (channel && archiveKeyList) {
            /* Loop all archive keys */
            var archiveFlag = false;
            async.each(archiveKeyList, function(_archiveKey, done) {
                /* Search data for archiving */
                db.PMN.WaterYearlyData.find({channel:channel, archiveKey:_archiveKey})
                .sort({timestamp:1})
                .exec(function(err, _archiveDataList) {
                    if (err) {
                        done(err);
                    } else {
                        if (_archiveDataList && _archiveDataList.length > 0) {
                            /* Archive data */
                            var archiveObj = {};
                            async.each(_archiveDataList, function(_archiveData, done2) {
                                archiveObj.channel = _archiveData.channel;
                                archiveObj.sumUnit =  (archiveObj.sumUnit) ?
                                    (parseFloat(archiveObj.sumUnit) + parseFloat(_archiveData.sumUnit)).toFixed(2) : _archiveData.sumUnit;
                                archiveObj.archiveDate = moment(_archiveKey, 'YYYY');
                                done2(null);
                            }, function(err) {
                                if (err) {
                                    done(err);
                                } else {
                                    /* Update archive data into database */
                                    db.PMN.WaterSummaryData.update({
                                        channel:archiveObj.channel,
                                        archiveDate:archiveObj.archiveDate
                                    },
                                    {$set:{
                                        channel:archiveObj.channel,
                                        sumUnit:archiveObj.sumUnit,
                                        archiveDate:archiveObj.archiveDate
                                    }},
                                    {upsert:true},
                                    function(err, stat) {
                                        if (err) {
                                            done(err);
                                        } else {
                                            if (stat.n > 0) archiveFlag = true;
                                            done(null);
                                        }
                                    });
                                }
                            });
                        } else {
                            done(null);
                        }
                    }
                });
            }, function(err) {
                if (err) {
                    callback(err);
                } else {
                    if (archiveFlag) {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.WaterYearlyData\' completed', '[' + channel + ']');
                    } else {
                        logger.debug('[' + pkg.name.toUpperCase() + ']', 'Archive \'pmn.WaterYearlyData\' ignored', '[' + channel + ']');
                    }
                    callback(null);
                }
            });
        } else {
            callback(null);
        }
    } catch (err) {
        callback(err);
    }
}

var clearWaterData = function() {
    try {
        setTimeout(clearWaterData, conf.subsys.pmn.clearArchiveInterval * 1000);
        /* Clear archived data */
        db.PMN.WaterData.remove({timestamp:{$lt:moment().subtract(1, 'years')}, 'archiveFlag':true}, function(err, stat) {
            if (err) {
                logger.error(err);
            } else {
                logger.info('[' + pkg.name.toUpperCase() + ']', 'Remove', stat.result.n, 'record(s)', 'of \'pmn.WaterData\'');
            }
        });
    } catch (err) {
        logger.error(err);
    }
}

var publishWaterDailyData = function() {
    try {
        setTimeout(publishWaterDailyData, conf.subsys.pmn.publishDataInterval * 1000);

        /* Prepare JSON schema */
        var waterDailyDataObj = {
            waterDailyData: {
                labels: [],
                datasets: {}
            }
        };

        /* Prepare date range */
        var now = new Date();
        var endDate = moment([now.getFullYear(), now.getMonth(), now.getDate(), now.getHours()]);
        var beginDate = endDate.clone().subtract(1, 'days');

        /* Get data & Prepare data */
        var runningDate = beginDate.clone().add(1, 'hours');
        var labels = [];
        var datasets = {};
        async.whilst(
            function() { return runningDate.isSameOrBefore(endDate); },
            function(done) {
                labels.push(runningDate.format('HH:mm'));
                var channelList = pmnConf.water.channel;
                async.each(channelList, function(_channel, done2) {
                    db.PMN.WaterDailyData.findOne({archiveDate:runningDate, channel:_channel})
                    .exec(function(err, _archiveData) {
                        if (err) {
                            done2(err);
                        } else {
                            var sumUnit = (_archiveData) ? parseFloat(_archiveData.sumUnit) : 0;
                            if (datasets[_channel]) {
                                datasets[_channel].push(sumUnit);
                            } else {
                                datasets[_channel] = [sumUnit];
                            }
                            done2(null);
                        }
                    });
                }, function(err) {
                    if (err) {
                        done(err);
                    } else {
                        /* Map object */
                        waterDailyDataObj.waterDailyData.timestamp = moment();
                        waterDailyDataObj.waterDailyData.labels = labels;
                        waterDailyDataObj.waterDailyData.datasets = datasets;
                        runningDate.add(1, 'hours');
                        done(null);
                    }
                });
            },
            function(err) {
                if (err) {
                    logger.error(err);
                } else {
                    emitter.emit('publishWaterDailyData', waterDailyDataObj);
                }
            }
        );
    } catch (err) {
        logger.error(err);
    }
}

var publishWaterMonthlyData = function() {
    try {
        setTimeout(publishWaterMonthlyData, conf.subsys.pmn.publishDataInterval * 1000);

        /* Prepare JSON schema */
        var waterMonthlyDataObj = {
            waterMonthlyData: {
                labels: [],
                datasets: {}
            }
        };

        /* Prepare date range */
        var endDate = moment().add(1, 'months').startOf('month');
        var beginDate = moment().startOf('month');

        /* Get data & Prepare data */
        var runningDate = beginDate.clone();
        var labels = [];
        var datasets = {};
        async.whilst(
            function() { return runningDate.isBefore(endDate); },
            function(done) {
                labels.push(runningDate.format('D'));
                var channelList = pmnConf.water.channel;
                async.each(channelList, function(_channel, done2) {
                    db.PMN.WaterMonthlyData.findOne({archiveDate:runningDate, channel:_channel})
                    .exec(function(err, _archiveData) {
                        if (err) {
                            done2(err);
                        } else {
                            var sumUnit = (_archiveData) ? parseFloat(_archiveData.sumUnit) : 0;
                            if (datasets[_channel]) {
                                datasets[_channel].push(sumUnit);
                            } else {
                                datasets[_channel] = [sumUnit];
                            }
                            done2(null);
                        }
                    });
                }, function(err) {
                    if (err) {
                        done(err);
                    } else {
                        /* Map object */
                        waterMonthlyDataObj.waterMonthlyData.timestamp = moment();
                        waterMonthlyDataObj.waterMonthlyData.labels = labels;
                        waterMonthlyDataObj.waterMonthlyData.datasets = datasets;
                        runningDate.add(1, 'days');
                        done(null);
                    }
                });
            },
            function(err) {
                if (err) {
                    logger.error(err);
                } else {
                    emitter.emit('publishWaterMonthlyData', waterMonthlyDataObj);
                }
            }
        );
    } catch (err) {
        logger.error(err);
    }
}

var publishWaterYearlyData = function() {
    try {
        setTimeout(publishWaterYearlyData, conf.subsys.pmn.publishDataInterval * 1000);

        /* Prepare JSON schema */
        var waterYearlyDataObj = {
            waterYearlyData: {
                labels: [],
                datasets: {}
            }
        };

        /* Prepare date range */
        var endDate = moment().add(1, 'years').startOf('year');
        var beginDate = moment().startOf('year');

        /* Get data & Prepare data */
        var runningDate = beginDate.clone();
        var labels = [];
        var datasets = {};
        async.whilst(
            function() { return runningDate.isBefore(endDate); },
            function(done) {
                labels.push(runningDate.format('MMM').toUpperCase());
                var channelList = pmnConf.water.channel;
                async.each(channelList, function(_channel, done2) {
                    db.PMN.WaterYearlyData.findOne({archiveDate:runningDate, channel:_channel})
                    .exec(function(err, _archiveData) {
                        if (err) {
                            done2(err);
                        } else {
                            var sumUnit = (_archiveData) ? parseFloat(_archiveData.sumUnit) : 0;
                            if (datasets[_channel]) {
                                datasets[_channel].push(sumUnit);
                            } else {
                                datasets[_channel] = [sumUnit];
                            }
                            done2(null);
                        }
                    });
                }, function(err) {
                    if (err) {
                        done(err);
                    } else {
                        /* Map object */
                        waterYearlyDataObj.waterYearlyData.timestamp = moment();
                        waterYearlyDataObj.waterYearlyData.labels = labels;
                        waterYearlyDataObj.waterYearlyData.datasets = datasets;
                        runningDate.add(1, 'months');
                        done(null);
                    }
                });
            },
            function(err) {
                if (err) {
                    logger.error(err);
                } else {
                    emitter.emit('publishWaterYearlyData', waterYearlyDataObj);
                }
            }
        );
    } catch (err) {
        logger.error(err);
    }
}

var publishWaterSummaryData = function() {
    try {
        setTimeout(publishWaterSummaryData, conf.subsys.pmn.publishDataInterval * 1000);

        /* Prepare JSON schema */
        var waterSummaryDataObj = {
            waterSummaryData: {
                labels: [],
                datasets: {}
            }
        };

        /* Prepare date range */
        var endDate = moment().add(1, 'years').startOf('year');
        var beginDate = moment().subtract(4, 'years').startOf('year');

        /* Get data & Prepare data */
        var runningDate = beginDate.clone();
        var labels = [];
        var datasets = {};
        async.whilst(
            function() { return runningDate.isBefore(endDate); },
            function(done) {
                labels.push(runningDate.format('YYYY').toUpperCase());
                var channelList = pmnConf.water.channel;
                async.each(channelList, function(_channel, done2) {
                    db.PMN.WaterSummaryData.findOne({archiveDate:runningDate, channel:_channel})
                    .exec(function(err, _archiveData) {
                        if (err) {
                            done2(err);
                        } else {
                            var sumUnit = (_archiveData) ? parseFloat(_archiveData.sumUnit) : 0;
                            if (datasets[_channel]) {
                                datasets[_channel].push(sumUnit);
                            } else {
                                datasets[_channel] = [sumUnit];
                            }
                            done2(null);
                        }
                    });
                }, function(err) {
                    if (err) {
                        done(err);
                    } else {
                        /* Map object */
                        waterSummaryDataObj.waterSummaryData.timestamp = moment();
                        waterSummaryDataObj.waterSummaryData.labels = labels;
                        waterSummaryDataObj.waterSummaryData.datasets = datasets;
                        runningDate.add(1, 'years');
                        done(null);
                    }
                });
            },
            function(err) {
                if (err) {
                    logger.error(err);
                } else {
                    emitter.emit('publishWaterSummaryData', waterSummaryDataObj);
                }
            }
        );
    } catch (err) {
        logger.error(err);
    }
}

/* Export modules */
module.exports.create = function() {
    try {
        /* Archive data */
        archiveElectricityData();
        archiveWaterData();

        /* Clear old archived data */
        clearElectricityData();
        clearWaterData();

        /* Publish data */
        publishElectricityDailyData();
        publishElectricityMonthlyData();
        publishElectricityYearlyData();
        publishElectricitySummaryData();
        publishWaterDailyData();
        publishWaterMonthlyData();
        publishWaterYearlyData();
        publishWaterSummaryData();

        return new PMN();

    } catch (err) {
        logger.error(err);
    }
}
