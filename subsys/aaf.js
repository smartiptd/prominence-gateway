'use strict';

/* Module dependencies */
var logger = require('smartdrive-lib').logger;
var EventEmitter = require('events').EventEmitter,
    emitter = new EventEmitter;
var config = require('config'),
    conf = config.get('app');

var AAF = function() {
    try {
        EventEmitter.call(this);
    } catch (err) {
        logger.error(err);
    }
};

AAF.prototype = emitter;

/*****************************
* Active AIRflow(TM) Process *
*****************************/
var updateData = function() {
    try {
        emitter.emit('data', { time: new Date() });
    } catch (err) {
        logger.error(err);
    }
};

/* Export modules */
module.exports.create = function() {
    try {
        setInterval(updateData, conf.subsys.aaf.updateDataInterval * 1000);
        return new AAF();
    } catch (err) {
        logger.error(err);
    }
}
