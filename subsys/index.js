'use strict';

/* Module dependencies */
var config = require('config'),
    conf = config.get('app');

/* Export modules */
module.exports.AAF = require('./aaf').create();
module.exports.PMN = require('./pmn').create();
