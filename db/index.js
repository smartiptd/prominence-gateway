'use strict';

/* Module dependencies */
var mongoose = require('mongoose');

/*******************
* Export DB Models *
*******************/
module.exports.AAF = require('./aaf');
module.exports.PMN = require('./pmn');

/**
* Convert String to ObjectId
*
* @param {String} str
*/
module.exports.parseObjectId = function(str) {
    return mongoose.Types.ObjectId(str);
}
