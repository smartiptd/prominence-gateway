'use strict';

/* Module dependencies */
var common = require('smartdrive-lib').common;
var constant = require('smartdrive-lib').constant;
var logger = require('smartdrive-lib').logger;
var pkg = require('../package');
var token = require('./token');
var async = require('async');
var jsonfile = require('jsonfile');
var fs = require('fs');
var mkdirp = require('mkdirp');
var request = require('request');
var config = require('config'),
    conf = config.get('app');

var subsysPath = common.getProjectPath() + conf.oauth.subsys.path;
var subsysFile = subsysPath + constant.SLASH + conf.oauth.subsys.file;

/* Exported module */
module.exports = {

    getSubsys: function(done) {
        try {
            async.parallel({
                current: function(callback) {
                    readSubsys(function(err, _subsys) {
                        callback(err, _subsys);
                    });
                },
                update: function(callback) {
                    newSubsys(function(err, _subsys) {
                        callback(err, _subsys);
                    });
                }
            }, function(err, results) {
                if (!results) {
                    logger.warn('[' + pkg.name.toUpperCase() + ']', 'Cannot get any subsys!');
                    return done(null, false);
                } else {
                    var current = results.current;
                    var update = results.update;
                    if (!current && update) {
                        mkdirp(subsysPath, function(err) {
                            if (err) {
                                return done(err);
                            } else {
                                jsonfile.writeFile(subsysFile, update, function(err) {
                                    if(err) {
                                        return done(err);
                                    } else {
                                        token.removeToken(function(err) {
                                            if (err) {
                                                return done(err);
                                            } else {
                                                logger.info('[' + pkg.name.toUpperCase() + ']', 'Update new subsys');
                                                return done(null, update);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    } else if (current && update) {
                        if (current.toString() != update.toString()) {
                            mkdirp(subsysPath, function(err) {
                                if (err) {
                                    return done(err);
                                } else {
                                    jsonfile.writeFile(subsysFile, update, function(err) {
                                        if(err) {
                                            return done(err);
                                        } else {
                                            token.removeToken(function(err) {
                                                if (err) {
                                                    return done(err);
                                                } else {
                                                    logger.info('[' + pkg.name.toUpperCase() + ']', 'Update new subsys');
                                                    return done(null, update);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        } else {
                            logger.info('[' + pkg.name.toUpperCase() + ']', 'Read old subsys');
                            return done(null, current);
                        }
                    } else if (current && !update) {
                        return done(null, current);
                    } else {
                        logger.warn('[' + pkg.name.toUpperCase() + ']', 'Cannot get any subsys!');
                        return done(null, false);
                    }
                }
            });
        } catch (err) {
            return done(err);
        }
    }
}

/* Local functions */
function readSubsys(done) {
    try {
        fs.stat(subsysFile, function(err, _stats) {
            if (err) {
                return done(null, false);
            } else {
                jsonfile.readFile(subsysFile, function(err, _subsys) {
                    return done(null, _subsys);
                });
            }
        });
    } catch (err) {
        return done(null, false);
    }
}

function newSubsys(done) {
    try {
        var options = {
            method: 'GET',
            url: conf.oauth.uri.subsysInfo,
            rejectUnauthorized: conf.oauth.options.rejectUnauthorized,
            qs: { client: conf.oauth.client.clientID },
            headers: { 'cache-control': 'no-cache' }
        };
        request(options, function (err, res, body) {
            if(err) {
                return done(null, false);
            } else {
                var _body = JSON.parse(body);
                if(!_body.subsys) {
                    return done(null, false);
                } else {
                    return done(null, _body.subsys);
                }
            }
        });
    } catch (err) {
        return done(null, false);
    }
}
