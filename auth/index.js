'use strict';

/* Module dependencies */
var common = require('smartdrive-lib').common;
var logger = require('smartdrive-lib').logger;
var pkg = require('../package');
var subsys = require('./subsys');
var token = require('./token');
var jwt = require('jsonwebtoken');
var client = require('./client');
var config = require('config'),
    conf = config.get('app');

var exports = module.exports = {};

/* Exported functions */
exports.getInfo = function(done) {
    try {
        subsys.getSubsys(function(err, _subsys) {
            if (err) {
                return done(err);
            } else {
                if (!_subsys) {
                    logger.warn('[' + pkg.name.toUpperCase() + ']', 'No information for authentication');
                    return done(null, false, false);
                } else {
                    token.readToken(function(err, _token) {
                        if (err) {
                            return done(err);
                        } else {
                            if(!_token) {
                                token.newToken(_subsys, function(err, _token) {
                                    if (err) {
                                        return done(err);
                                    } else {
                                        if (!_token) {
                                            logger.warn('[' + pkg.name.toUpperCase() + ']', 'No token for authentication');
                                            return done(null, _subsys, false);
                                        } else {
                                            getChallengeNumber(_token, function(err, _challengeNum) {
                                                if (err) {
                                                    return done(err);
                                                } else {
                                                    if (!_challengeNum) {
                                                        logger.warn('[' + pkg.name.toUpperCase() + ']', 'No token for authentication');
                                                        return done(null, _subsys, false);
                                                    } else {
                                                        logger.info('[' + pkg.name.toUpperCase() + ']', 'Get new token');
                                                        _token.challenge_num = _challengeNum;
                                                        return done(null, _subsys, _token);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                if (new Date().getTime() > _token.expires_date) {
                                    logger.warn('[' + pkg.name.toUpperCase() + ']', 'Token expired');
                                    token.updateToken(_token.refresh_token, function(err, _token) {
                                        if (err) {
                                            return done(err);
                                        } else {
                                            if (!_token) {
                                                logger.warn('[' + pkg.name.toUpperCase() + ']', 'No token for authentication');
                                                return done(null, _subsys, false);
                                            } else {
                                                getChallengeNumber(_token, function(err, _challengeNum) {
                                                    if (err) {
                                                        return done(err);
                                                    } else {
                                                        if (!_challengeNum) {
                                                            logger.warn('[' + pkg.name.toUpperCase() + ']', 'No token for authentication');
                                                            return done(null, _subsys, false);
                                                        } else {
                                                            logger.info('[' + pkg.name.toUpperCase() + ']', 'Update token with refresh token');
                                                            _token.challenge_num = _challengeNum;
                                                            return done(null, _subsys, _token);
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });
                                } else {
                                    getChallengeNumber(_token, function(err, _challengeNum) {
                                        if (err) {
                                            return done(err);
                                        } else {
                                            if (!_challengeNum) {
                                                logger.warn('[' + pkg.name.toUpperCase() + ']', 'No token for authentication');
                                                return done(null, _subsys, false);
                                            } else {
                                                logger.info('[' + pkg.name.toUpperCase() + ']', 'Read old token');
                                                _token.challenge_num = _challengeNum;
                                                return done(null, _subsys, _token);
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            }
        });
    } catch (err) {
        return done(err);
    }
}

exports.removeToken = token.removeToken;

exports.clientAuthenticate = function() {
    return client.clientAuthenticate();
}
exports.clientAuthorizePublish = function() {
    return client.clientAuthorizePublish();
}
exports.clientAuthorizeSubscribe = function() {
    return client.clientAuthorizeSubscribe();
}

/* Local functions */
function getChallengeNumber(_token, done) {
    try {
        if (_token) {
            var jwtHeader = conf.oauth.jwt.header,
                jwtPayload = _token.access_token,
                jwtVerify = _token.refresh_token;
            if (jwtHeader && jwtPayload && jwtVerify) {
                var jwtToken = jwtHeader + '.' + jwtPayload + '.' + jwtVerify;
                jwt.verify(jwtToken, conf.oauth.client.clientSecret, function(err, _decoded) {
                    if (err) {
                        return done(err);
                    } else {
                        if (!_decoded.chl) {
                            return done(null, false);
                        } else {
                            var encryptedRandNum = _decoded.chl;
                            var decryptedRandNum = common.decrypt(encryptedRandNum, conf.oauth.client.clientSignature);
                            return done(null, decryptedRandNum);
                        }
                    }
                });
            } else {
                return done(null, false);
            }
        } else {
            return done(null, false);
        }
    } catch (err) {
        return done(err);
    }
}
