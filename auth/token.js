'use strict';

/* Module dependencies */
var constant = require('smartdrive-lib').constant;
var common = require('smartdrive-lib').common;
var async = require('async');
var jsonfile = require('jsonfile');
var fs = require('fs');
var mkdirp = require('mkdirp');
var request = require('request');
var config = require('config'),
    conf = config.get('app');

var tokenPath = common.getProjectPath() + conf.oauth.token.path;
var tokenFile = tokenPath + constant.SLASH + conf.oauth.token.file;

/* Exported module */
module.exports = {

    readToken: function(done) {
        fs.stat(tokenFile, function(err, _stats) {
            if (err) {
                if ('ENOENT' == err.code) {
                    return done(null, false);
                } else {
                    return done(err);
                }
            } else {
                jsonfile.readFile(tokenFile, function(err, _token) {
                    return done(err, _token);
                });
            }
        });
    },

    newToken: function(subsys, done) {
        try {
            var _scope = constant.BLANK;
            async.each(subsys, function(_subsys, cb) {
                var _tmpScope = conf.mqtt.client.scope;
                _tmpScope = _tmpScope.replace('<SUBSYSTYPE>', _subsys.type);
                _tmpScope = _tmpScope.replace('<SUBSYSID>', _subsys.id);
                _scope += (_tmpScope + ';');
                cb();
            }, function(err) {
                if(err) {
                    return done(err);
                } else {
                    var _tmpScope = conf.mqtt.client.commonScope;
                    _tmpScope = _tmpScope.replace('<CLIENTID>', conf.oauth.client.clientID);
                    _scope += (_tmpScope + ';');
                    var options = {
                        method: 'POST',
                        url: conf.oauth.uri.token,
                        rejectUnauthorized: conf.oauth.options.rejectUnauthorized,
                        headers: {
                            'content-type': 'application/x-www-form-urlencoded',
                            'cache-control': 'no-cache'
                        },
                        form: {
                            grant_type: 'client_credentials',
                            client_id: conf.oauth.client.clientID,
                            client_secret: conf.oauth.client.clientSecret,
                            scope: _scope
                        }
                    };
                    request(options, function (err, res, _body) {
                        if (err) {
                            return done(err);
                        } else {
                            var _token = JSON.parse(_body);
                            mkdirp(tokenPath, function(err) {
                                if(err) {
                                    return done(err);
                                } else {
                                    if (!_token || !_token.access_token) {
                                        return done(null, false);
                                    } else{
                                        var access_token = _token.access_token,
                                            refresh_token = _token.refresh_token,
                                            expires_date = new Date().getTime() + (_token.expires_in * 1000),
                                            token_type = _token.token_type;
                                        var exportToken = {
                                            'access_token': access_token,
                                            'refresh_token': refresh_token,
                                            'expires_date': expires_date,
                                            'token_type': token_type
                                        };
                                        jsonfile.writeFile(tokenFile, exportToken, function(err) {
                                            if(err) {
                                                return done(err);
                                            } else {
                                                return done(null, exportToken);
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    });
                }
            });
        } catch (err) {
            return done(err);
        }
    },

    updateToken: function(refreshToken, done) {
        var options = {
            method: 'POST',
            url: conf.oauth.uri.token,
            rejectUnauthorized: conf.oauth.options.rejectUnauthorized,
            headers: {
                'content-type': 'application/x-www-form-urlencoded',
                'cache-control': 'no-cache'
            },
            form: {
                grant_type: 'refresh_token',
                client_id: conf.oauth.client.clientID,
                client_secret: conf.oauth.client.clientSecret,
                refresh_token: refreshToken
            }
        };
        request(options, function (err, res, _body) {
            if (err) {
                return done(err);
            } else {
                var _token = JSON.parse(_body);
                mkdirp(tokenPath, function(err) {
                    if(err) {
                        return done(err);
                    } else {
                        if (!_token || !_token.access_token) {
                            return done(null, false);
                        } else{
                            var access_token = _token.access_token,
                                refresh_token = _token.refresh_token,
                                expires_date = new Date().getTime() + (_token.expires_in * 1000),
                                token_type = _token.token_type;
                            var exportToken = {
                                'access_token': access_token,
                                'refresh_token': refresh_token,
                                'expires_date': expires_date,
                                'token_type': token_type
                            };
                            jsonfile.writeFile(tokenFile, exportToken, function(err) {
                                if(err) {
                                    return done(err);
                                } else {
                                    return done(null, exportToken);
                                }
                            });
                        }
                    }
                });
            }
        });
    },

    removeToken: function(done) {
        fs.stat(tokenFile, function(err, _stats) {
            if (err) {
                if ('ENOENT' == err.code) {
                    return done();
                } else {
                    return done(err);
                }
            } else {
                fs.unlink(tokenFile, function(err) {
                    if (err) {
                        return done(err);
                    } else {
                        return done();
                    }
                });
            }
        });
    }
}
