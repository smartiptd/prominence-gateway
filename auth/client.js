'use strict';

/* Module dependencies */
var config = require('config'),
    conf = config.get('app');

var exports = module.exports = {};

/*********************************
* Authentication & Authorization *
*********************************/

/* Exported functions */
exports.clientAuthenticate = function() {
    return function(client, username, password, callback) {
        try {
            var authResult = (username == conf.mqtt.broker.credential.username &&
                password.toString() == conf.mqtt.broker.credential.password) ? true : false;
            return callback(null, authResult);
        } catch (err) {
            return callback(err);
        }
    }
}

exports.clientAuthorizePublish = function() {
    return function(client, topic, payload, callback) {
        try {
            return callback(null, true);
        } catch (err) {
            return callback(err);
        }
    }
}

exports.clientAuthorizeSubscribe = function() {
    return function(client, topic, callback) {
        try {
            return callback(null, true);
        } catch (err) {
            return callback(err);
        }
    }
}
